﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestCase.DataModel;

namespace TestCase
{
    public partial class StaffForm : Form
    {
        List<Staff> Staff;
        Staff tempStaff;
        public StaffForm(List<Staff> s)
        {
            InitializeComponent();
            Staff = s;
            
            listBox1.DataSource = Staff;
            tempStaff = new Staff();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tempStaff.FullName = textBox1.Text;
            tempStaff.Rank = textBox2.Text;
            tempStaff.Position = textBox3.Text;
            tempStaff.CombatNumber = textBox4.Text;
            Staff.Add(tempStaff);
            listBox1.DataSource = null;
            listBox1.DataSource = Staff;
            tempStaff = new Staff();
        }
    }
}
