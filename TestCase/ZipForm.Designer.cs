﻿namespace TestCase
{
    partial class ZipForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.ConsumablesListBox = new System.Windows.Forms.ListBox();
            this.repairListBox = new System.Windows.Forms.ListBox();
            this.toolsListBox = new System.Windows.Forms.ListBox();
            this.consumablesTextBox = new System.Windows.Forms.TextBox();
            this.consumablesButton = new System.Windows.Forms.Button();
            this.repairTextBox = new System.Windows.Forms.TextBox();
            this.repairButton = new System.Windows.Forms.Button();
            this.toolsTextBox = new System.Windows.Forms.TextBox();
            this.toolsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.consumablesButton);
            this.splitContainer1.Panel1.Controls.Add(this.consumablesTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.ConsumablesListBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1021, 443);
            this.splitContainer1.SplitterDistance = 340;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.repairButton);
            this.splitContainer2.Panel1.Controls.Add(this.repairTextBox);
            this.splitContainer2.Panel1.Controls.Add(this.repairListBox);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.toolsButton);
            this.splitContainer2.Panel2.Controls.Add(this.toolsTextBox);
            this.splitContainer2.Panel2.Controls.Add(this.toolsListBox);
            this.splitContainer2.Size = new System.Drawing.Size(677, 443);
            this.splitContainer2.SplitterDistance = 341;
            this.splitContainer2.TabIndex = 0;
            // 
            // ConsumablesListBox
            // 
            this.ConsumablesListBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.ConsumablesListBox.FormattingEnabled = true;
            this.ConsumablesListBox.Location = new System.Drawing.Point(0, 0);
            this.ConsumablesListBox.Margin = new System.Windows.Forms.Padding(5);
            this.ConsumablesListBox.Name = "ConsumablesListBox";
            this.ConsumablesListBox.Size = new System.Drawing.Size(340, 368);
            this.ConsumablesListBox.TabIndex = 0;
            // 
            // repairListBox
            // 
            this.repairListBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.repairListBox.FormattingEnabled = true;
            this.repairListBox.Location = new System.Drawing.Point(0, 0);
            this.repairListBox.Margin = new System.Windows.Forms.Padding(5);
            this.repairListBox.Name = "repairListBox";
            this.repairListBox.Size = new System.Drawing.Size(341, 368);
            this.repairListBox.TabIndex = 0;
            // 
            // toolsListBox
            // 
            this.toolsListBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolsListBox.FormattingEnabled = true;
            this.toolsListBox.Location = new System.Drawing.Point(0, 0);
            this.toolsListBox.Margin = new System.Windows.Forms.Padding(5);
            this.toolsListBox.Name = "toolsListBox";
            this.toolsListBox.Size = new System.Drawing.Size(332, 368);
            this.toolsListBox.TabIndex = 0;
            // 
            // consumablesTextBox
            // 
            this.consumablesTextBox.Location = new System.Drawing.Point(4, 376);
            this.consumablesTextBox.Name = "consumablesTextBox";
            this.consumablesTextBox.Size = new System.Drawing.Size(334, 20);
            this.consumablesTextBox.TabIndex = 1;
            // 
            // consumablesButton
            // 
            this.consumablesButton.Location = new System.Drawing.Point(262, 402);
            this.consumablesButton.Name = "consumablesButton";
            this.consumablesButton.Size = new System.Drawing.Size(75, 23);
            this.consumablesButton.TabIndex = 2;
            this.consumablesButton.Text = "Добавить";
            this.consumablesButton.UseVisualStyleBackColor = true;
            this.consumablesButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // repairTextBox
            // 
            this.repairTextBox.Location = new System.Drawing.Point(3, 376);
            this.repairTextBox.Name = "repairTextBox";
            this.repairTextBox.Size = new System.Drawing.Size(335, 20);
            this.repairTextBox.TabIndex = 1;
            // 
            // repairButton
            // 
            this.repairButton.Location = new System.Drawing.Point(263, 402);
            this.repairButton.Name = "repairButton";
            this.repairButton.Size = new System.Drawing.Size(75, 23);
            this.repairButton.TabIndex = 2;
            this.repairButton.Text = "Добавить";
            this.repairButton.UseVisualStyleBackColor = true;
            this.repairButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // toolsTextBox
            // 
            this.toolsTextBox.Location = new System.Drawing.Point(3, 376);
            this.toolsTextBox.Name = "toolsTextBox";
            this.toolsTextBox.Size = new System.Drawing.Size(326, 20);
            this.toolsTextBox.TabIndex = 1;
            // 
            // toolsButton
            // 
            this.toolsButton.Location = new System.Drawing.Point(254, 402);
            this.toolsButton.Name = "toolsButton";
            this.toolsButton.Size = new System.Drawing.Size(75, 23);
            this.toolsButton.TabIndex = 2;
            this.toolsButton.Text = "Добавить";
            this.toolsButton.UseVisualStyleBackColor = true;
            this.toolsButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // ZipForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 443);
            this.Controls.Add(this.splitContainer1);
            this.Name = "ZipForm";
            this.Text = "ZipForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListBox ConsumablesListBox;
        private System.Windows.Forms.ListBox repairListBox;
        private System.Windows.Forms.ListBox toolsListBox;
        private System.Windows.Forms.Button consumablesButton;
        private System.Windows.Forms.TextBox consumablesTextBox;
        private System.Windows.Forms.Button repairButton;
        private System.Windows.Forms.TextBox repairTextBox;
        private System.Windows.Forms.Button toolsButton;
        private System.Windows.Forms.TextBox toolsTextBox;
    }
}