﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.Serialization;
using TestCase.DataModel;
using TestCase.Extentions;

namespace TestCase
{
    public partial class Form1 : Form
    {
        XmlSerializer serializer;
        public Form1DataModel DataModel { get; set; }
        public Form1()
        {
            InitializeComponent();
            serializer = new XmlSerializer(typeof(Form1DataModel));
            DataModel = new Form1DataModel();
          
        }

     
        private void RefreshStaff()
        {
            equipmentControl1.mainComboBox.DataSource = DataModel.Staff;
            equipmentControl1.mainComboBox.DisplayMember = "FullName";

            equipmentControl1.mainComboBox.SelectedIndex = -1;

            equipmentControl1.viceComboBox.DataSource = DataModel.Staff.Clone();
            equipmentControl1.viceComboBox.DisplayMember = "FullName";

            equipmentControl1.viceComboBox.SelectedIndex = -1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RefreshTreeView();
        }

        private void RefreshTreeView()
        {
            equipmentTreeView.Nodes.Clear();
            DataModel.FullEquipment.ForEach(x => equipmentTreeView.Nodes.Add(x.GenerateTreeNode()));
        }


        private void equipmentTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.equipmentControl1.Item = e.Node.Tag as Equipment;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Xml files(*.xml)|*.xml";
            fileDialog.ShowDialog();
            Serialize(fileDialog.FileName);
        }

        private void Serialize(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }
            StringBuilder sb = new StringBuilder();

            using (TextWriter textwriter = new StringWriter(sb))
            {
                serializer.Serialize(textwriter, DataModel);
            }
            using (StreamWriter writer = new StreamWriter(filePath,false,Encoding.Unicode))
            {
                writer.WriteLine(sb.ToString());
                writer.Flush();
                writer.Close();
            }


        }

        private void Deserialize(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }
            Stream s = new FileStream(filePath, FileMode.Open);
            
            try
            {
                var tempData = serializer.Deserialize(s) as Form1DataModel;
                if (tempData != null)
                {
                    DataModel = tempData;
                }
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException != null)
                {
                    MessageBox.Show(ex.InnerException.Message);
                }
            }
            finally
            {
                s.Flush();
                s.Close();
            }

            RefreshStaff();
            RefreshTreeView();
            SetMaintenanceTypes(filePath);
        }

        private void SetMaintenanceTypes(string filePath)
        {
            Stream fileStream = new FileStream(filePath, FileMode.Open);
            XDocument document = XDocument.Load(fileStream);
            IEnumerable<XElement> types = from doc in document.Descendants("MaintenanceType")
                                         select doc;
            MaintenanceForm.MaintenanceTypes.AddRange(types.Select(x=>x.Value));
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Xml files(*.xml)|*.xml";
            fileDialog.ShowDialog();
            Deserialize(fileDialog.FileName);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы хотите сохранить данные?", string.Empty, MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Save();
            }
            Application.Exit();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (this.equipmentTreeView.SelectedNode != null)
            {
                var dd = (this.equipmentTreeView.SelectedNode.Tag as Equipment);
                dd.Components.Add(new Equipment());
            }
            else
            {
                this.DataModel.FullEquipment.Add(new Equipment());
            }
            RefreshTreeView();
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshTreeView();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataModel = new Form1DataModel();
            equipmentControl1.Item = new Equipment();
            RefreshStaff();
            RefreshTreeView();
        }

        private void addToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            StaffForm f = new StaffForm(DataModel.Staff);
            f.ShowDialog();
            RefreshStaff();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.equipmentTreeView.SelectedNode.MoveUp();
            UpdateEquipment();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.equipmentTreeView.SelectedNode.MoveDown();
            UpdateEquipment();
        }

        private void UpdateEquipment()
        {
            if (this.equipmentTreeView.SelectedNode.Parent != null)
            {
                Equipment tag = (this.equipmentTreeView.SelectedNode.Tag as Equipment).Parent;
                tag.Components.Clear();
                foreach (TreeNode it in this.equipmentTreeView.SelectedNode.Parent.Nodes)
                {
                    tag.Components.Add(it.Tag as Equipment);
                }
            }
            else
            {
                DataModel.FullEquipment.Clear();
                foreach (TreeNode it in this.equipmentTreeView.Nodes)
                {
                    DataModel.FullEquipment.Add(it.Tag as Equipment);
                }
            }
        }
    }
}
