﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestCase.DataModel;

namespace TestCase.Controls
{
    public partial class EquipmentControl : UserControl
    {
        private Equipment item;
        public EquipmentControl()
        {
            InitializeComponent();
            
        }
        
        public Equipment Item
        {
            get
            {
                return item;
            }
            set
            {
               
                item = value;
                Init();
            }
        }


        private void Init()
        {
            if (item == null)
            {
                return;
            }
            this.nameTextBox.Text = item.Name;
            this.codeTextBox.Text = item.Code;
            this.descriptionTextBox.Text = item.Description;
            if (item.ResponsiblePerson != null)
            {
                this.mainComboBox.SelectedIndex = mainComboBox.FindStringExact(item.ResponsiblePerson.FullName);
            }
            else
            {
                this.mainComboBox.SelectedIndex = -1;
            }
            if (item.ViceResponsiblePerson != null)
            {
                this.viceComboBox.SelectedIndex = viceComboBox.FindStringExact(item.ViceResponsiblePerson.FullName);
            }
            else
            {
                this.viceComboBox.SelectedIndex = -1;
            }
            this.maintenanceDataGridView.AutoGenerateColumns = false;
            
            this.maintenanceDataGridView.DataSource = item.MaintenanceWorks;
            
            
            
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            var changedTextBox = sender as TextBox;
            switch (changedTextBox.Name)
            {
                case "nameTextBox":
                    item.Name = changedTextBox.Text;
                    break;
                case "codeTextBox":
                    item.Code = changedTextBox.Text;
                    break;
                case "descriptionTextBox":
                    item.Description = changedTextBox.Text;
                    break;
                default:
                    break;
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (item == null)
            {
                return;
            }
            var changedTextBox = sender as ComboBox;
            switch (changedTextBox.Name)
            {
                case "mainComboBox":
                    item.ResponsiblePerson = changedTextBox.SelectedValue as Staff;
                    break;
                case "viceComboBox":
                    item.ViceResponsiblePerson = changedTextBox.SelectedValue as Staff;
                    break;
               
                default:
                    break;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                ZipForm z = new ZipForm((maintenanceDataGridView.SelectedRows[0].DataBoundItem as Maintenance).MaintenanceZip);
                z.ShowDialog();
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var m = new Maintenance();
            MaintenanceForm f = new MaintenanceForm(m);
            f.ShowDialog();
            item.MaintenanceWorks.Add(m);
            maintenanceDataGridView.DataSource = null;
            maintenanceDataGridView.DataSource = item.MaintenanceWorks;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MaintenanceForm f = new MaintenanceForm((maintenanceDataGridView.SelectedRows[0].DataBoundItem as Maintenance));
            f.ShowDialog();
            maintenanceDataGridView.DataSource = null;
            maintenanceDataGridView.DataSource = item.MaintenanceWorks;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow it in maintenanceDataGridView.SelectedRows)
            {
                item.MaintenanceWorks.Remove(it.DataBoundItem as Maintenance);
            }
            maintenanceDataGridView.DataSource = null;
            maintenanceDataGridView.DataSource = item.MaintenanceWorks;
        }
    }
}
