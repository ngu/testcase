﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestCase.DataModel;

namespace TestCase
{
    public partial class ZipForm : Form
    {
        Zip item;
        public ZipForm(Zip zip)
        {
            InitializeComponent();
            item = zip;
            InitializeData();
        }

        private void InitializeData()
        {
            this.ConsumablesListBox.DataSource = item.Consumables;
            this.repairListBox.DataSource = item.RepairParts;
            this.toolsListBox.DataSource = item.Tools;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var clickedButton = sender as Button;
            
            switch (clickedButton.Name)
            {
                case "consumablesButton":
                    item.Consumables.Add(consumablesTextBox.Text);
                    ConsumablesListBox.DataSource = null;
                    ConsumablesListBox.DataSource = item.Consumables;
                    break;
                case "repairButton":
                    item.RepairParts.Add(repairTextBox.Text);
                    repairListBox.DataSource = null;
                    repairListBox.DataSource = item.RepairParts;
                    break;
                case "toolsButton":
                    item.Tools.Add(toolsTextBox.Text);
                    toolsListBox.DataSource = null;
                    toolsListBox.DataSource = item.Tools;
                    break;
                default:
                    break;
            }
        }
    }
}
