﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestCase.DataModel;

namespace TestCase
{
    public partial class MaintenanceForm : Form
    {
        public static List<string> MaintenanceTypes 
        {
            get;
            set;
        }

        static MaintenanceForm()
        {
            MaintenanceTypes = new List<string>();
        }

        public static void AddMaintenanceType(string mt)
        {
            if (string.IsNullOrEmpty(mt))
            {
                return;
            }
            if (!MaintenanceTypes.Contains(mt))
            {
                MaintenanceTypes.Add(mt);
            }
        }

        Maintenance item;
        public MaintenanceForm(Maintenance m)
        {
            InitializeComponent();
            item = m;
            comboBox1.DataSource = Maintenance.Periods;
            comboBox2.DataSource = MaintenanceTypes;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            item.Name = textBox1.Text;
            item.Description = textBox3.Text;
            item.MaintenanceType = comboBox2.Text;
            item.PeriodString = comboBox1.Text;
            float.TryParse(textBox4.Text, out float f);
            item.LaborIntensity = f;
            AddMaintenanceType(comboBox2.Text);
            
            this.Close();
        }

        private void MaintenanceForm_Load(object sender, EventArgs e)
        {
            comboBox2.DataSource = null;
            comboBox2.DataSource = MaintenanceTypes;
            textBox1.Text = item.Name;
            if (!string.IsNullOrEmpty(item.MaintenanceType))
            {
                comboBox2.Text = item.MaintenanceType;
            }
            else
            {
                comboBox2.SelectedIndex = -1;
            }
            textBox3.Text = item.Description;
            textBox4.Text = item.LaborIntensity.ToString();
            comboBox1.SelectedIndex = (int)item.Period;
            
            
        }
    }
}
