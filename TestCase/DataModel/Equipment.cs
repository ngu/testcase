﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace TestCase.DataModel
{
    [XmlRoot("Equipment")]
    public class Equipment
    {
        public Equipment()
        {

            MaintenanceWorks = new List<Maintenance>();
            Components = new List<Equipment>();
        }

        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("Code")]
        public string Code { get; set; }

        [XmlElement("Description")]
        public string Description { get; set; }

        [XmlElement("ResponsiblePerson")]
        public Staff ResponsiblePerson { get; set; }

        [XmlElement("ViceResponsiblePerson")]
        public Staff ViceResponsiblePerson { get; set; }

        [XmlArray("MaintenanceWorks", IsNullable = true), XmlArrayItem(typeof(Maintenance), ElementName = "Maintenance")]
        public List<Maintenance> MaintenanceWorks { get; set; }

        [XmlArray("Components", IsNullable = true), XmlArrayItem(typeof(Equipment),ElementName ="Equipment")]
        public List<Equipment> Components { get; set; }
        [XmlIgnore()]
        public Equipment Parent { get; private set; }

        internal TreeNode GenerateTreeNode()
        {
            TreeNode node = new TreeNode();
            node.Text = Name;
            node.Tag = this;
            Components.ForEach(x => node.Nodes.Add(x.GenerateTreeNode(this)));
            return node;
        }

        internal TreeNode GenerateTreeNode(Equipment parent)
        {
            TreeNode node = new TreeNode();
            node.Text = Name;
            node.Tag = this;
            this.Parent = parent;
            Components.ForEach(x => node.Nodes.Add(x.GenerateTreeNode()));
            return node;
        }
    }
}
