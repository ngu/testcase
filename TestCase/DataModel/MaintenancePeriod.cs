﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TestCase.DataModel
{
    public enum MaintenancePeriod
    {
        /// <summary>
        /// Раз в неделю
        /// </summary>
        OnceAWeek=0,

        /// <summary>
        /// Раз в месяц
        /// </summary>
        OnceAMonth = 1,

        /// <summary>
        /// Раз в полголда
        /// </summary>
        HalfYearly = 2,

        /// <summary>
        /// Раз в год
        /// </summary>
        OnceAYear = 3,

        /// <summary>
        /// Через 100 часов
        /// </summary>
        AfterAHundredHours = 4,

        /// <summary>
        /// Раз в неделю
        /// </summary>
        AfterAThousandHours = 5
       
    }
}
