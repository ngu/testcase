﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TestCase.DataModel
{
    [XmlRoot("Zip")]
    public class Zip
    {
        public Zip()
        {
            Consumables = new List<string>();
            RepairParts = new List<string>();
            Tools = new List<string>();
        }

        [XmlArray("Consumables",IsNullable =true),XmlArrayItem("Consumable",Type =typeof(string))]
        public List<string> Consumables
        {
            get;
            set;
        }
        [XmlArray("RepairParts", IsNullable = true), XmlArrayItem("RepairPart", Type = typeof(string))]
        public List<string> RepairParts
        {
            get;
            set;
        }

        [XmlArray("Tools", IsNullable = true), XmlArrayItem("Tool", Type = typeof(string))]
        public List<string> Tools
        {
            get;
            set;
        }
    }
}
