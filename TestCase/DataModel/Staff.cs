﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TestCase.DataModel
{
    [XmlRoot("Stuff")]
    public class Staff:ICloneable
    {
        

        [XmlElement("Id")]
        public int Id { get; set; }

        [XmlElement("FullName")]
        public string FullName { get; set; }

        [XmlElement("Rank")]
        public string Rank { get; set; }

        [XmlElement("Position")]
        public string Position { get; set; }

        [XmlElement("CombatNumber")]
        public string CombatNumber { get; set; }

        public object Clone()
        {
            Staff result = new Staff()
            {
                Id = this.Id,
                CombatNumber = this.CombatNumber,
                FullName = this.FullName,
                Position = this.Position,
                Rank = this.Rank
            };
            return result;
        }
        public override string ToString()
        {
            return FullName; 
        }
    }
}
