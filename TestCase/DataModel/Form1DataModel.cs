﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TestCase.DataModel
{
    [XmlRoot("DataModel")]
    public class Form1DataModel
    {
        
        public Form1DataModel()
        {
            FullEquipment = new List<Equipment>();
            Staff = new List<Staff>();
           
        }

        [XmlArray("EquipmentList", IsNullable = true), XmlArrayItem(typeof(Equipment), ElementName = "Equipment")]
        public List<Equipment> FullEquipment
        {
            get;
            set;
        }

        [XmlArray("Staff", IsNullable = true), XmlArrayItem(typeof(Staff), ElementName = "StaffMember")]
        public List<Staff> Staff
        {
            get;
            set;
        }

        
    }
}
