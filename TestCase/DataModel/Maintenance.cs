﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TestCase.DataModel
{
    public class Maintenance
    {
        public static readonly List<string> Periods = new List<string> { "Раз в неделю", "Раз в месяц", "Раз в полгода", "Раз в год", "Через 100 часов", "Через 1000 часов" };

        public Maintenance()
        {
            MaintenanceZip = new Zip();
        }
        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("MaintenanceType")]
        public string MaintenanceType { get; set; }

        [XmlElement("Description")]
        public string Description { get; set; }

        [XmlElement("MaintenancePeriod")]
        public MaintenancePeriod Period { get; set; }

        [XmlElement("LaborIntensity")]
        public float LaborIntensity { get; set; }

        [XmlElement("MaintenanceZip", Type =typeof(Zip))]
        public Zip MaintenanceZip { get; set; }

        [XmlIgnore()]
        public string PeriodString
        {
            get
            {
                return Periods[(int)Period];
            }
            set
            {
                if (Periods.Contains(value))
                {
                    Period = (MaintenancePeriod)Periods.IndexOf(value);
                }
            }
        }
    }
}
